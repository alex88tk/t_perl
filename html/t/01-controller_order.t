#!/usr/bin/perl

use strict;
use warnings;

use File::Basename;
use Data::Dumper;
use base qw(Test::Class);
use Test::More tests => 25;
use Test::MockObject;

use lib dirname(__FILE__) . '/../src';
use Controller::Order;
use Entity::Order;
use Entity::User;
use JSON;

my ( %data, $fakeCGI, $fakeJournal, $fakeModel );

sub startup : Test(startup=>0) {
    %data = (
        'perf_id' => '',
        'name'    => '',
        'price'   => '',
        'amount'  => ''
    );

    $fakeCGI     = Test::MockObject->new();
    $fakeJournal = Test::MockObject->new();
    $fakeCGI->mock(
        'param',
        sub {
            return encode_json \%data;
        }
    );

    $fakeJournal->mock( 'save', sub { } );

    $fakeModel = Test::MockObject->new();
    $fakeModel->mock( 'save',         sub { 1 } );
    $fakeModel->mock( 'searchName',   sub { } );
    $fakeModel->mock( 'searchNumber', sub { } );
}

sub testUpdatePerf_id : Test(3) {

    my $controllerOrder = Controller::Order->new($fakeCGI);
    $data{'perf_id'} = '';
    is(
        $controllerOrder->update(),
        'perf_id must be int AND > 0',
        'good check perf_id'
    );
    $data{'perf_id'} = -22;
    is(
        $controllerOrder->update(),
        'perf_id must be int AND > 0',
        'good check perf_id'
    );
    $data{'perf_id'} = 22;
    isnt(
        $controllerOrder->update(),
        'perf_id must be int AND > 0',
        'good check perf_id'
    );
}

sub testUpdateName : Test(2) {

    my $controllerOrder = Controller::Order->new($fakeCGI);
    %data = (
        'perf_id' => 99,
        'name'    => '',
    );
    is( $controllerOrder->update(), 'name must be init', 'good check name' );
    $data{'name'} = 'Test';
    isnt( $controllerOrder->update(), 'name must be init', 'good check name' );
}

sub testUpdatePrice : Test(5) {
    %data = (
        'perf_id' => 99,
        'name'    => 'Test',
        'price'   => ''
    );

    my $controllerOrder = Controller::Order->new($fakeCGI);

    is(
        $controllerOrder->update(),
        'price must be float AND > 0',
        'good check price'
    );

    $data{'price'} = 0;
    is(
        $controllerOrder->update(),
        'price must be float AND > 0',
        'good check price'
    );

    $data{'price'} = -123.32;
    is(
        $controllerOrder->update(),
        'price must be float AND > 0',
        'good check price'
    );

    $data{'price'} = 123.32;
    isnt(
        $controllerOrder->update(),
        'price must be float AND > 0',
        'good check price'
    );

    $data{'price'} = 100;
    isnt(
        $controllerOrder->update(),
        'price must be float AND > 0',
        'good check price'
    );
}

sub testUpdateAmount : Test(5) {
    %data = (
        'perf_id' => 10,
        'name'    => 'Test',
        'price'   => 17,
        'amount'  => '',
    );

    my $controllerOrder = Controller::Order->new($fakeCGI);

    is(
        $controllerOrder->update(),
        'amount must be int AND > 0',
        'good check amount'
    );
    $data{'amount'} = 0;
    is(
        $controllerOrder->update(),
        'amount must be int AND > 0',
        'good check amount'
    );
    $data{'amount'} = -10;
    is(
        $controllerOrder->update(),
        'amount must be int AND > 0',
        'good check amount'
    );
    $data{'amount'} = 22.4;
    is(
        $controllerOrder->update(),
        'amount must be int AND > 0',
        'good check amount'
    );
    $data{'amount'} = 11;
    isnt(
        $controllerOrder->update(),
        'amount must be int AND > 0',
        'good check amount'
    );
}

sub testUpdateNumber : Test(5) {
    %data = (
        'perf_id' => 10,
        'name'    => 'Test',
        'price'   => 17,
        'amount'  => 10,
    );

    my $controllerOrder =
      Controller::Order->new( $fakeCGI, $fakeModel, $fakeModel, $fakeModel,
        $fakeJournal );

    is(
        $controllerOrder->update(),
        'order number must be int AND > 0',
        'good check number'
    );
    is(
        $controllerOrder->update(''),
        'order number must be int AND > 0',
        'good check number'
    );
    is(
        $controllerOrder->update(-123),
        'order number must be int AND > 0',
        'good check number'
    );
    is(
        $controllerOrder->update(14.66),
        'order number must be int AND > 0',
        'good check number'
    );
    isnt(
        $controllerOrder->update(14),
        'order number must be int AND > 0',
        'good check number'
    );
}

sub testUpdateOrderSearchNumber : Test(4) {
    %data = (
        'perf_id' => 10,
        'name'    => 'Test',
        'price'   => 17,
        'amount'  => 10,
    );

    my $modelOrder = Test::MockObject->new();
    $modelOrder->mock( 'searchNumber', sub { } );
    $modelOrder->mock( 'update',       sub { } );

    #return id
    $modelOrder->mock( 'save', sub { 11 } );

    my $modelAction = Test::MockObject->new();

    #@type Entity::Action
    my $action = undef;
    $modelAction->mock(
        'save',
        sub {
            my ( $self, $msg ) = @_;
            $action = $msg;
            return 11;
        }
    );

    my $modelUser = Test::MockObject->new();
    $modelUser->mock(
        'searchName',
        sub {
            my $user = Entity::User->new();
            $user->setId(1);
            $user->setName('test');
            return $user;
        }
    );

    my $controllerOrder =
      Controller::Order->new( $fakeCGI, $modelOrder, $modelUser, $modelAction,
        $fakeJournal );

    is( $controllerOrder->update(33), 1, 'good check add order Number' );
    can_ok( $action, 'getInfo' );
    is(
        $action->getInfo,
        'create new order with number: 33',
        'good check add order Number'
    );

    $modelOrder->mock(
        'searchNumber',
        sub {
            my $order = Entity::Order->new();
            $order->setId(1);
            return $order;
        }
    );
    $controllerOrder->update(33);
    is(
        $action->getInfo,
        'update new order with number: 33',
        'good check add order Number'
    );
}

sub testAddUser : Test(1) {
    %data = (
        'perf_id' => 10,
        'name'    => 'TestName',
        'price'   => 17,
        'amount'  => 10,
    );

    my $modelAction = Test::MockObject->new();

    #@type Entity::Action
    my $action = undef;
    $modelAction->mock(
        'save',
        sub {
            my ( $self, $msg ) = @_;
            $action = $msg;
            return 11;
        }
    );

    my $controllerOrder =
      Controller::Order->new( $fakeCGI, $fakeModel, $fakeModel, $modelAction,
        $fakeJournal );

    $controllerOrder->update(33);
    is( $action->getInfo, 'add new user:TestName', 'good check add User' );
}

Test::Class->runtests;
