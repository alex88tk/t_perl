package Model::Order;

use warnings;
use strict;
use DBI;
use Data::Dumper;
use Entity::Order;

sub new {
    my $class = shift;
    return bless(
        {
            #@type DBI
            _dbh => shift
        },
        $class
    );
}

#@returns Entity::Order
sub searchNumber {
    my ( $self, $number ) = @_;
    my $sth =
      $self->{_dbh}->prepare(
'Select id, number, perf_id, price, amount FROM orders WHERE number = ? LIMIT 1'
      );
    $sth->execute($number);

    if ( my $item = $sth->fetchrow_hashref() ) {
        my $order = Entity::Order->new();
        $order->setId( $item->{'id'} );
        $order->setNumber( $item->{'number'} );
        $order->setPerfId( $item->{'perf_id'} );
        $order->setPrice( $item->{'price'} );
        $order->setAmount( $item->{'amount'} );
        return $order;
    }
    return undef;
}

sub save {
    my (
        $self,

        #@type Entity::Order
        $order
    ) = @_;
    my $sth = $self->{_dbh}->prepare(
        'INSERT INTO orders(number, perf_id, price, amount) VALUES (?,?,?,?)');
    $sth->execute(
        $order->getNumber(), $order->getPerfId,
        $order->getPrice(),  $order->getAmount
    );

    return $self->{_dbh}->last_insert_id();
}

sub update {
    my (
        $self,

        #@type Entity::Order
        $order
    ) = @_;

    my $sth = $self->{_dbh}->prepare(
        'UPDATE orders SET number = ?, perf_id = ?, price = ?, amount = ?');
    $sth->execute(
        $order->getNumber(), $order->getPerfId,
        $order->getPrice(),  $order->getAmount
    );
}

1;
